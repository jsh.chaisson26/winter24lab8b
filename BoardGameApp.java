import java.util.Scanner;
public class BoardGameApp
{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Board board = new Board();
		System.out.println("Welcome to my fun game!");
		int numCastles = 7;
		int turns = 0;
		while(numCastles > 0 && turns < 8){
			System.out.println("What row would you like to place your token?");
			int row = Integer.parseInt(reader.nextLine())-1;
			System.out.println("What column would you like to place your token?");
			int col = Integer.parseInt(reader.nextLine())-1;
			System.out.println("There are " + numCastles + " castles!");
			System.out.println("Turns: " + turns);
			System.out.println(board);
			int token = board.placeToken(row, col);
			while(token < 0){
				System.out.println("The values you have written are out of bounds for this game. Try again.");
				System.out.println("Turns: " + turns);
				System.out.println("There are " + numCastles + " castles!");
				System.out.println("What row would you like to place your token?");
				row = Integer.parseInt(reader.nextLine())-1;
				System.out.println("What column would you like to place your token?");
				col = Integer.parseInt(reader.nextLine())-1;
				token = board.placeToken(row, col);
			}
			if(token == 1){
				System.out.println("There was wall where you placed your token.");
				turns++;
				System.out.println("Turns: " + turns);
			}
			if(token == 0){
				System.out.println("Castle tile successfully placed.");
				turns++;
				numCastles --;
				System.out.println("Turns: " + turns);
				System.out.println("There are " + numCastles + " castles!");
			}
		}
		System.out.println(board);
		if(numCastles == 0){
			System.out.println("YOU WON");
		}
		else{
			System.out.println("YOU LOST");
		}
	}
}