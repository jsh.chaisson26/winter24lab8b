import java.util.Random;
public class Board
{
	private Tile[][] grid;
	private final int SIZE =5;
	
	public Board(){ //constructor makes every spot a blank and puts a hidden castle in each of the rows(5  hidden castles total).
		Random rng = new Random();
		this.grid = new Tile[SIZE][SIZE];
		Tile tiles = Tile.BLANK;
		for(int i =0; i < this.grid.length; i++){
			int randIndex = rng.nextInt(this.grid.length);
			for(int j=0; j < this.grid.length; j++){
				if(j == randIndex){
					this.grid[i][j] = Tile.HIDDEN_WALL;
				}
				else{
					this.grid[i][j] = tiles;
				}
			}
		}
	}

	public String toString(){ //allows display to look the way it is( all blank spots).
		String output = "";
		for(int i =0; i < this.grid.length; i++){
			for(int j=0; j < this.grid.length; j++){
				output += this.grid[i][j].getName() + " ";
			}
			output +=  "\n";
		}
		return output;
	}
	public int placeToken(int row, int col){ // checks what type of token the current position is on and changes it and or returns something based on that token.
		if(col > this.SIZE || row > this.SIZE){
			return -2;
		}
		else if(this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL){
			return -1;
		}
		else if(this.grid[row][col] == Tile.HIDDEN_WALL){
			this.grid[row][col] = Tile.WALL;
			return 1;
		}
		else{
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
}


































































